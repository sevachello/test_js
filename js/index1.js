var activeElement = null;

function init() {
    document.addEventListener("DOMContentLoaded", function () {
        var navigationElement = document.getElementById('navigation');
        navigationElement.addEventListener("mouseover", handlerMouse);
        navigationElement.addEventListener("click", showRectNumber);
    });
}

function setActive(){
    activeElement.className = 'active';
}

function removeActive(){
    activeElement.className = '';
}

function showRectNumber() {
    console.log('Click at rect ' + activeElement.getAttribute('data-rect'));
}

function handlerMouse(event) {
    if (activeElement !== null) {
        removeActive();
    }
    activeElement = event.target;
    setActive();
}

init();