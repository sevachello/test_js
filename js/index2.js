var keyActiveElement = 0;
var navigationChildren = null;

function init() {
    document.addEventListener("DOMContentLoaded", function () {
        var navigationElement = document.getElementById('navigation');
        navigationChildren = navigationElement.children;
        if (navigationChildren.length > 0) {
            navigationElement.addEventListener("click", showRectNumber);
            document.addEventListener("keypress", handleKeyPress);
            setActive();
        }
    });
}

function setActive(elementKey){
    var key = elementKey === undefined ? keyActiveElement : elementKey;
    navigationChildren[key].className = 'active';
}

function removeActive(elementKey){
    var key = elementKey === undefined ? keyActiveElement : elementKey;
    navigationChildren[key].className = '';
}

function showRectNumber() {
    console.log('Click at rect ' + navigationChildren[keyActiveElement].getAttribute('data-rect'));
}

function moveVertical(direction) {
    switch (direction) {
        case 'up':
            if (keyActiveElement > 2 && keyActiveElement < 7){
                keyActiveElement = 0;
            } else if (keyActiveElement > 6 && keyActiveElement < 10) {
                keyActiveElement = 4;
            } else if (keyActiveElement > 9 && keyActiveElement < 13) {
                keyActiveElement = 3;
            }
            break;
        case 'down':
            if (keyActiveElement < 3) {
                keyActiveElement = 3;
            } else if (keyActiveElement > 3 && keyActiveElement < 7){
                keyActiveElement = 7;
            } else if ((keyActiveElement > 6 && keyActiveElement < 10) || keyActiveElement === 3) {
                keyActiveElement = 10;
            }
            break;
    }
}

function handleKeyPress(event) {
    var oldActive = keyActiveElement;
    var lastElement = navigationChildren.length - 1;
    switch (event.keyCode) {
        case 38: // up
            moveVertical('up');
            break;
        case 39: // right
            keyActiveElement++;
            break;
        case 40: // down
            moveVertical('down');
            break;
        case 37: // left
            keyActiveElement--;
            break;
        case 13: // enter
            showRectNumber();
            break;
    }

    if (event.keyCode === 13) {
        return false;
    }

    if (keyActiveElement < 0) {
        keyActiveElement = 0;
    } else if(keyActiveElement > lastElement) {
        keyActiveElement = lastElement;
    } else {
        removeActive(oldActive);
        setActive();
    }
}

init();