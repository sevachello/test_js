// TODO 1. write description 2. expand api and comments 3. add control by mouse
class RectNavigation {
    /**
     *
     * @param settings
     */
    constructor(settings)
    {
        this._navElement = this._handleField(settings.element, null);
        this._items = this._handleField(settings.items, []);
        this._size = this._handleField(settings.size, 50);
        this._margin = this._handleField(settings.margin, 25);
        this._activeItem = this._handleField(settings.activeItem, 0);
        this._control = {
            keyboard: this._handleField(settings.keyboard, true),
            mouse: this._handleField(settings.mouse, false),
        };

        this._errors = {
            0: 'Data is not object',
            1: 'Empty data',
            2: 'Parent element not exist',
            3: 'Invalid activeItem value',
        };

        this._keyboardKeys = {
            38: 'up',
            39: 'right',
            40: 'down',
            37: 'left'
        };

        this.init();
    }

    /**
     *
     * @returns {{keyboard: boolean, mouse: boolean}}
     */
    get control()
    {
        return this._control;
    }

    /**
     *
     * @returns {array}
     */
    get items ()
    {
        return this._items;
    }

    /**
     *
     * @returns {null|object}
     */
    get navElement()
    {
        return this._navElement;
    }

    /**
     *
     * @returns {number}
     */
    get activeItem()
    {
        return this._activeItem;
    }

    /**
     * create items and add event for move at items
     */
    init()
    {
        if (typeof this._navElement !== "object") {
            throw this._errors[2];
        }

        if (typeof this._items !== "object") {
            throw this._errors[0];
        }

        if (this._items.length < 1) {
            throw this._errors[1];
        }

        let patternActiveItem = /^[0-9]\d*$/;
        if ((!patternActiveItem.test(this._activeItem)) || this.activeItem > this._items.length-1){
            throw this._errors[3];
        }

        this.buildElementForNavigation();

        if (this._control.keyboard) {
            this._handleKeyBoard();
        }

        if (this._control.mouse) {
            this._handleMouse();
        }
    }

    /**
     * creation elements for navigation element and set active for first item
     * @private
     */
    buildElementForNavigation()
    {
        for (let i in this._items) {
            let item = this._createItemElement(i, this._items[i]);
            this._navElement.appendChild(item);
        }
        this._setActiveItem();
    }

    /**
     *
     * @param key
     * @param itemProperty
     * @returns {HTMLDivElement}
     * @private
     */
    _createItemElement(key, itemProperty)
    {
        let item = document.createElement('div');
        item.setAttribute('data-rect', key);
        item.rectKey = parseInt(key);

        if (itemProperty.height !== undefined && typeof itemProperty.height === "number") {
            item.style.height = this._size * itemProperty.height + 'px';
        } else {
            item.style.height = this._size + 'px';
        }

        if (itemProperty.width !== undefined && typeof itemProperty.width === "number") {
            item.style.width = this._size * itemProperty.width + 'px';
        } else {
            item.style.width = this._size + 'px';
        }

        item.style.margin = this._margin + 'px';

        if (itemProperty.marginLeft !== undefined && typeof itemProperty.marginLeft === "number") {
            item.style.marginLeft = (this._size * itemProperty.marginLeft + this._margin) + 'px';
        }

        if (itemProperty.marginRight !== undefined && typeof itemProperty.marginRight=== "number") {
            item.style.marginRight = (this._size * itemProperty.marginRight + this._margin) + 'px';
        }

        item.itemProperty = itemProperty;
        item.moves = [];
        for (let i in itemProperty) {
            if (typeof itemProperty[i] === "number") {
                item.moves.push(itemProperty[i]);
            }
        }

        return item;
    }

    /**
     * displays the active item number
     */
    displayInfoToConsole()
    {
        let rectId = this._navElement.children[this._activeItem].getAttribute('data-rect');
        console.log('Click at rect ' + rectId);
    }

    /**
     *
     * @param itemKey
     * @private
     */
    _setActiveItem(itemKey)
    {
        if (typeof itemKey !== "number") {
            itemKey = this._activeItem;
        } else {
            this._activeItem = itemKey;
        }
        this._navElement.children[itemKey].classList.add('active');
    }

    /**
     *
     * @param itemKey
     * @private
     */
    _removeActiveItem(itemKey)
    {
        if (typeof itemKey !== "number") {
            itemKey = this._activeItem;
        }
        this._navElement.children[itemKey].classList.remove('active');
    }

    /**
     * handle control by keyboard
     * @private
     */
    _handleKeyBoard()
    {
        let self =  this;
        let handleMove = function (event) {
            if (event.keyCode === 13) {
                self.displayInfoToConsole();
            }

            // if press not on list: up,right,down and left
            if ([37, 38, 39, 40].indexOf(event.keyCode) === -1) {
                return false;
            }

            let itemMoves = self._items[self._activeItem];
            let requestMove = itemMoves[self._keyboardKeys[event.keyCode]];

            if (typeof requestMove !== "number"){
                return false;
            }

            self._removeActiveItem();
            self._setActiveItem(requestMove);
        };
        if (this._control.keyboard){
            document.addEventListener('keypress', handleMove);
        } else {
            document.removeEventListener('keypress', handleMove);
        }
    }

    /**
     * method can enable\disable control by keyboard
     * @param enable {boolean}
     */
    setKeyboardStatus(enable)
    {
        if (typeof value === "boolean" && this._control.keyboard !== enable) {
            this._control.keyboard = enable;
            this._handleKeyBoard();
        }
    }

    /**
     * handle control by mouse
     * @private
     */
    _handleMouse()
    {
        let self = this;
        let handleMove = function (event) {
            if (event.target !== self._navElement) {
                self._navElement.children[self._activeItem].moves.forEach(function (value) {
                    if (event.target.rectKey === value) {
                        self._removeActiveItem();
                        self._setActiveItem(event.target.rectKey);
                    }
                });
            }
        };

        let handleClick = function (event) {
            if (event.target !== self._navElement && event.target === self._navElement.children[self._activeItem]) {
                self.displayInfoToConsole();
            }
        };

        if (this._control.mouse){
            this._navElement.addEventListener("mouseover", handleMove);
            this._navElement.addEventListener("click", handleClick);
        } else {
            this._navElement.removeEventListener('mouseover', handleMove);
            this._navElement.removeEventListener('click', handleClick);
        }
    }

    /**
     * method can enable\disable control by mouse
     * @param enable {boolean}
     */
    setMouseStatus(enable)
    {
        if (typeof value === "boolean" && this._control.mouse !== enable) {
            this._control.mouse = enable;
            this._handleMouse();
        }
    }

    /**
     * return defaultValue if field is undefined
     * @param field
     * @param defaultValue
     * @returns {*}
     * @private
     */
    _handleField(field, defaultValue)
    {
        return field === undefined ? defaultValue : field;
    }

}